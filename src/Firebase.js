import { initializeApp } from "firebase/app";
import {
  GoogleAuthProvider,
  getAuth,
  signInWithPopup,
  signOut,
} from "firebase/auth";
import {
  getFirestore,
  query,
  getDocs,
  collection,
  where,
  addDoc,
} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBcRmYn-AAqY_wzDIYIjfghz5tNw4fqAes",
  authDomain: "superchat-3c075.firebaseapp.com",
  projectId: "superchat-3c075",
  storageBucket: "superchat-3c075.appspot.com",
  messagingSenderId: "152011228144",
  appId: "1:152011228144:web:1d502d0a3c036b4c3d1335",
  measurementId: "G-7K0WJ3T096"
  };
  
// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp)
const db = getFirestore()
    
// Auth config
auth.useDeviceLanguage()
const googleProvider = new GoogleAuthProvider();


const signInWithGoogle = async () => {
  try {
    const res = await signInWithPopup(auth, googleProvider);
    const user = res.user;
    console.log(user)
    const q = query(collection(db, "users"), where("uid", "==", user.uid));
    const docs = await getDocs(q);
    if (docs.docs.length === 0) {
      await addDoc(collection(db, "users"), {
        uid: user.uid,
        name: user.displayName,
        authProvider: "google",
        email: user.email,
      });
    }
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};

const logout = () => {
  console.log('logging out')
  signOut(auth);
};

export {
  auth,
  db,
  signInWithGoogle,
  logout,
  firebaseApp
};