import firebase from 'firebase'
import firebaseui from 'firebaseui'


// инициализация 
const ui = new firebaseui.auth.AuthUI(firebase.auth())
// Настройка провайдеров
ui.start('#firebase-auth-container', {
    signInOptions: [
        {
            provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
            requireDisplayName: true
        },
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.GithubAuthProvider.PROVIDER_ID
    ]
})
