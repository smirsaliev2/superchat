import React from 'react'
import { useNavigate } from 'react-router-dom'
import Header from '../components/Header'
import MessagesFeed from '../components/MessagesFeed'
import Send from '../components/Send'

function Chat({ user }) {
    const navigate = useNavigate()
    if (!user) navigate('/login')
    return (
        <>
            <Header />
            <MessagesFeed userID={user.uid}/>
            <Send user={user}/>
        </>
    )
}

export default Chat