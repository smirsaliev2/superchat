import React, { useEffect } from 'react'
import styled from 'styled-components'

const LoadingScreen = styled.div`
backgroung-color: grey;
width: 100vw;
height: 100vh;
color: var(--main);
font-size: 3rem;
display: flex;
text-align: center;
justify-content: center;
align-items: center;
padding: 40px;`

function Home() {
    return (
        <LoadingScreen className='loading'>
            С наступающим НГ🎊
        </LoadingScreen>
    )
}

export default Home