import React from "react";
import { GoogleAuthProvider, EmailAuthProvider } from 'firebase/auth'
import { auth } from '../Firebase.js'
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';

// Конфигурация
const uiConfig = {
  signInSuccessUrl: '/chat',
  signInFlow: 'popup',
  signInOptions: [
      {
          provider: EmailAuthProvider.PROVIDER_ID,
          requireDisplayName: true
      },
      GoogleAuthProvider.PROVIDER_ID
  ]    
}
function Login() {
  return (
    <div className="Login">
      <h1 className="Login__header">Superchat</h1>
      <p className="Login__subheader">Залогиньтесь:</p>
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={auth} />
    </div>
  )
}

export default Login