import React, { useEffect} from 'react'
import { Routes, Route, useNavigate } from 'react-router-dom'
import Chat from './pages/Chat'
import Login from "./pages/Login"
import Home from './pages/Home'
import { auth, signInWithGoogle } from "./Firebase.js";
import { useAuthState } from "react-firebase-hooks/auth";

import './App.css'

function App() {
  const [user, loading, error] = useAuthState(auth)
  const navigate = useNavigate()
  useEffect(() => {
    if (loading) {
      return
    }
    const timeout = setTimeout(() => {
      if(user) {
        navigate('/chat')}
      else navigate('/login')
      }, 500)

    return () => {
      clearTimeout(timeout)
    }
  }, [user, loading])
  
  return (
    <div className="App">
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route exact path='/login' element={<Login />}/>
        {user && <Route exact path='/chat' element={<Chat user={user}/>} />}
      </Routes>
    </div>
  )
}

export default App