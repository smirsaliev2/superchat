import React from 'react'
import Message from './Message.js'
import { firebaseApp } from '../Firebase.js'
import { getFirestore, collection, query, orderBy } from 'firebase/firestore'
import { useCollection } from 'react-firebase-hooks/firestore'

function MessagesFeed({ userID }) {
    const messagesRef = collection(getFirestore(firebaseApp), 'messages')
    const q = query(messagesRef, orderBy('timestamp'))
    const [messages, loading, error] = useCollection(q, {idField: 'id'})
    return (
        <>
           <div className='MessagesFeed'>
            {messages && messages.docs.map((doc) => {
                return <Message 
                    key={doc.data().id}
                    msg={doc.data()} 
                    userID={userID}
                />
            })}

         </div>
        </>
    )
}

export default MessagesFeed