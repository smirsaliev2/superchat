import React from 'react'

function Message({ msg, userID }) {
    const user = userID === msg.userID
        ? 'Message--user' 
        : 'Message--enemy'

    
    const avatar = msg.userAvatarURL
        ? `url(${msg.userAvatarURL})`
        : 'orange'
        
    const avatarStyle = {
        background: avatar,
        backgroundSize: 'cover'
    }

    return (
        <div className={`Message ${user}`}>
            <div className='Message__avatar' style={avatarStyle}></div>
            <div className={`Message__content ${user}`}>
                {user=='Message--enemy' && <p className='Message__username'>{msg.username}</p>}
                <p className='Message__text'>{msg.message}</p>
            </div>
        </div>
    )
}

export default Message