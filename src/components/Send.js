import React, { useState } from 'react'
import { db } from '../Firebase'
import { addDoc, collection, serverTimestamp } from "firebase/firestore";

function Send({ user }) {
  const [message, setMessage] = useState('')

  function handleInput(e) {
    const value = e.target.value
    setMessage(value)
  }

  async function sendMessage(e) {
    try {
      e.preventDefault()
      addDoc(collection(db, "messages"), {
        userID: user.uid,
        username: user.displayName, 
        userAvatarURL: user.photoURL,
        message: message,
        timestamp: serverTimestamp()
      })
    } catch(err) {
      console.log(err)
    }
  }

  return (
    <div className='Send'>
      <form className='message-form'>
        <input 
          className='message-input' 
          placeholder='напиши что-нибудь хорошее'
          onChange={handleInput}
          value={message}
        >
          </input>
          <button 
            className='message-btn'
            onClick={sendMessage}
          >
            Отправить
          </button>
      </form>
    </div>
  )
}

export default Send