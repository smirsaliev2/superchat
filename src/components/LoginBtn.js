import React from "react";

function LoginBtn({ name, clickHandler }) {
  return (
      <button 
        className='login-btn auth-btn'
        onClick={clickHandler}
      >
        {`Войти через ${name}`}
      </button>
  )
}

export default LoginBtn