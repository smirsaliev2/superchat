import React from 'react'
import { logout } from '../Firebase'

function Header() {
    return (
        <div className='Header container'>
            <h1 className='logo'>Суперчат</h1>
            <button 
                className='logout auth-btn'
                onClick={logout}
            >
                Выйти
            </button>
        </div>
    )
}

export default Header